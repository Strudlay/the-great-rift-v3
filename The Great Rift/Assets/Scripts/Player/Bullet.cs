﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour{
    float bulletSpeed = 100f;
    float lifespan = 3f;
    Rigidbody rigidbody;
    private void Awake() {
        rigidbody = GetComponent<Rigidbody>();
    }
    private void Update() {
        rigidbody.AddForce(rigidbody.transform.right * bulletSpeed);
        
    }
}
