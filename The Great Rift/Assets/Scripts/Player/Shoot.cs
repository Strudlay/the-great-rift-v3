﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField] GameObject bullet;
    [SerializeField] Transform leftGun, rightGun;
    private void Start() {
        
    }
    void Update() {
        Fire();
    }
    void Fire() {
        if (Input.GetButtonDown("Fire1")) {
            SpawnBullets();
        }
    }
    private void SpawnBullets() {
        Instantiate(bullet, leftGun.position, Quaternion.identity);
        Instantiate(bullet, rightGun.position, Quaternion.identity);
    }
}
