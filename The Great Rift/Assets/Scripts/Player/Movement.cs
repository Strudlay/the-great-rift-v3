﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    bool leftKey, rightKey, upKey, downKey;
    Vector3 playerPos, targetAngle, currentAngle, currentEulerAngle;
    float hsp, vsp, speed, rotationSpeed, maxRotation, rotationZ;
    private void Awake() {
        
    }
    private void Start() {
        speed = 1f;
        vsp = 0;
        hsp = 0;
        targetAngle = new Vector3(0, 90f, 0);
        rotationSpeed = 4f;
        maxRotation = 45f;
        rotationZ = 0;
    }
    private void Update() {
        CalculateDireciton();
        ApplyDirection();
        //ApplyAngle();
        PlayerClamp();
    }
    private void PlayerClamp() {
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp(pos.x, 0.07f, 0.95f);
        pos.y = Mathf.Clamp(pos.y, 0.05f, 0.95f);
        transform.position = Camera.main.ViewportToWorldPoint(pos);
    }
    private void CalculateDireciton() {
        leftKey = Input.GetKey(KeyCode.A);
        rightKey = Input.GetKey(KeyCode.D);
        upKey = Input.GetKey(KeyCode.W);
        downKey = Input.GetKey(KeyCode.S);
        hsp = (System.Convert.ToInt32(rightKey) - System.Convert.ToInt32(leftKey));
        vsp = (System.Convert.ToInt32(upKey) - System.Convert.ToInt32(downKey));
    }
    private void ApplyDirection() {
        playerPos = transform.position;
        playerPos.x += hsp * speed;
        playerPos.y += vsp * speed;
        transform.position = playerPos;
    }
    private void ApplyAngle() {
        currentAngle = transform.eulerAngles;
        currentEulerAngle = transform.rotation.eulerAngles;
        Vector3 rotationDirection = new Vector3(0, 0, 1);
        if (vsp > 0) {
            print(transform.eulerAngles.z);
            if(transform.eulerAngles.z <= maxRotation) {
                transform.Rotate(rotationDirection, Space.Self);
            }
        }
        else if (vsp < 0) {
            // Needs to be fixed
            if (transform.eulerAngles.z <= -maxRotation) {
                transform.Rotate(-rotationDirection, Space.Self);
            }
            print(transform.eulerAngles.z - maxRotation);
        }
        if (vsp == 0) {
            currentAngle = new Vector3(
             Mathf.LerpAngle(currentAngle.x, targetAngle.x, Time.deltaTime * rotationSpeed),
             Mathf.LerpAngle(currentAngle.y, targetAngle.y, Time.deltaTime * rotationSpeed),
             Mathf.LerpAngle(currentAngle.z, targetAngle.z, Time.deltaTime * rotationSpeed));
            transform.eulerAngles = currentAngle;
        }

    }
}
